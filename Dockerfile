FROM python:3.7


COPY . /opt/app
RUN mkdir /opt/app/media
RUN pip3 install uwsgi
RUN pip3 install -r /opt/app/requirements.txt
ENV DJANGO_ENV=prod
ENV DOCKER_CONTAINER=1
ENV PYTHONUNBUFFERED 1

EXPOSE 8000
