from django.contrib import admin
from .models import Image

@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'place', 'size')
    readonly_fields = ('image_tag', 'created_at')
