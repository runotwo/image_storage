from django.db import models
from django.utils.html import mark_safe


class Image(models.Model):
    image = models.ImageField(upload_to='images/%Y/%m/%d')
    created_at = models.DateTimeField(auto_now_add=True)
    place = models.CharField(max_length=255)
    size = models.IntegerField()

    def save(self, *args, **kwargs):
        self.size = self.image.size
        super().save(*args, **kwargs)

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % (self.image))

    image_tag.short_description = 'Image'
