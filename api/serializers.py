from rest_framework import serializers
from .models import Image
import base64
from PIL import Image as pil_Image
from django.core.files import File
from uuid import uuid4
import io


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        exclude = ('id', 'image')
        read_only_fields = ('account_name', 'size')

    path_to_img = serializers.SerializerMethodField(read_only=True)
    img = serializers.CharField(write_only=True)
    created_at = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S", read_only=True)

    def get_path_to_img(self, obj):
        return self.context['request'].build_absolute_uri(obj.image.url)

    def validate_img(self, img):
        try:
            pil_image = pil_Image.open(io.BytesIO(base64.b64decode(img)))
        except Exception:
            raise serializers.ValidationError('Must be base64-encoded image.')
        img = File(io.BytesIO(base64.b64decode(img)))
        img.name = f'{uuid4()}.{pil_image.format.lower()}'
        return img
