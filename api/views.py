from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .models import Image
from .serializers import ImageSerializer
from django.utils.dateparse import parse_date


class ImageView(APIView):
    def post(self, request, format=None):
        serializer = ImageSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        Image.objects.create(image=data['img'], place=data['place'], size=data['img'].size)
        return Response(status=status.HTTP_201_CREATED)

    def get(self, request, format=None):
        qs = Image.objects.all()
        filters = {}
        for filter in request.query_params:
            if filter in ('date', 'date__gt', 'date__lt', 'date__gte', 'date__lte'):
                try:
                    filters[filter.replace('date', 'created_at')] = parse_date(request.query_params[filter])
                except ValueError:
                    pass
            if filter in ('size', 'size__gt', 'size__lt', 'size__gte', 'size__lte'):
                try:
                    filters[filter] = int(request.query_params[filter])
                except ValueError:
                    pass
        qs = qs.filter(**filters)
        serializer = ImageSerializer(qs, many=True, context={'request': request})
        return Response(data=serializer.data)
